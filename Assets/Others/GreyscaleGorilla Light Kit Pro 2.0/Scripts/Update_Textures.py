import c4d
import string
import os
from c4d import gui
#Welcome to the world of Python
def GetShader(shader):
    CheckShader(shader)
    if shader.GetNext() != None:
        GetShader(shader.GetNext())
    if shader.GetDown() !=None:
        GetShader(shader.GetDown())
        
def CheckShader(shader):
    if shader.GetType() == 5833:#Bitmap
       CheckParameter(shader, c4d.BITMAPSHADER_FILENAME)
       filename = shader[c4d.BITMAPSHADER_FILENAME]
       if filename==None:
            return
                   

       if string.find(filename, "greyscalegorillahdrilightkitpro1.5.lib4d",0,len(filename)) == -1:
            return
       head,tail =os.path.split(filename)
       shader[c4d.BITMAPSHADER_FILENAME] = tail
    
def CheckParameter(shader,ID):
    shader[ID]
def main():
    mats = doc.GetMaterials()
    for mat in mats:
        shader = mat.GetFirstShader()
        if shader==None: return
        GetShader(shader)
if __name__=='__main__':
    main()
